// Check-off specific Todo by clicking
$("ul").on("click", "li", function(){
  $(this).toggleClass("completed");
});

// Click on "X" to delete Todo
$("ul").on("click", "span", function(){
  $(this).parent().fadeOut(500, function(){
    $(this).remove();
  });
  event.stopPropagation();
});

$("input[type='text']").keypress(function(){
  // If ENTER
  if(event.which === 13){
    var todoText = $(this).val();
    // Create Toto
    $("ul").append("<li><span><i class='fa fa-trash'></i></span> " + todoText + "</li>");
    $(this).val("");
  }
});

$(".fa-plus").click(function(){
  $("input[type='text']").fadeToggle(0.1);
});